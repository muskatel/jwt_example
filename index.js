/**
 * Dependencies
 * @ignore
 */
const { Issuer, generators } = require("openid-client");
const express = require("express");

/**
 * Configuration
 * @ignore
 */
const redirect_uri = "http://localhost:8080/callback";
const app = express();
let issuer;
let client;

async function configure() {
  issuer = await Issuer.discover(
    "https://gitlab.com/.well-known/openid-configuration"
  );

  // The issuer provides a document that tells your client how to configure itself. This is called "discovery".
  console.log("Discovered issuer %s %O", issuer.issuer, issuer.metadata);

  client = new issuer.Client({
    client_id: process.env.CLIENT_ID || "hard coded client id", // hard coding is bad, use env rather
    client_secret: process.env.CLIENT_SECRET || "hard coded client secret",
    redirect_uris: [redirect_uri],
    response_types: ["code"],
  });

  /**
   * Endpoints
   * @ignore
   */
  app.get("/", (req, res) => {
    res.redirect(
      client.authorizationUrl({
        scope: "openid email profile",
      })
    );
  });

  app.get("/callback", async (req, res) => {
    const params = client.callbackParams(req);
    const tokenSet = await client.callback(redirect_uri, params);

    console.log("Received and validated tokens %j", tokenSet);
    console.log("Token Claims %j", tokenSet.claims());

    res.status(204).send();
  });

  app.listen(process.env.PORT || 8080, () => {
    console.log("Listening");
  });
}

/**
 * Run
 * @ignore
 */
configure();
